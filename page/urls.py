from django.urls import re_path
from . import views

#url for app
urlpatterns = [
    re_path(r'^$', views.KamilaStory, name='KamilaStory'),
    # re_path(r'^contact/$', views.contact, name='contact'),
    re_path(r'^art/$', views.Art, name='Art'),
    re_path(r'^form/$', views.form, name='form'),
    re_path(r'^show/$', views.show, name='show'),
    re_path(r'^show/$', views.show, name='show'),
    re_path(r'^delete-activity/(?P<pk>\d+)/$', views.delete_activity, name='delete_activity'),
]